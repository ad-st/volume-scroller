﻿using System;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Linq;
using System.Threading;
using System.Drawing;


namespace VolumeScroller
{
    static class Program
    {
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            using (var main = new Main())
            {
                Application.Run();
            }
        }   
    }
}
