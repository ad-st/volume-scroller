﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VolumeScroller
{
    class Icon : IDisposable
    {
        public enum Icons
        {
            Normal,
            Up,
            Down
        };

        public Icon()
        {
            _icon.Text = "Volume Scroller";
            _icon.Icon = VolumeScroller.Properties.Resources.mouse;
            _icon.Visible = true;
            _icon.DoubleClick += OnDblClick;

            _autoResetTimer = new System.Timers.Timer
            {
                Interval = 1000,
                AutoReset = true
            };
            _autoResetTimer.Elapsed += OnAutoReset;
        }

        public void SetIcon(Icons type)
        {
            switch (type)
            {
                default:
                case Icons.Normal:
                    _icon.Icon = VolumeScroller.Properties.Resources.mouse;
                    break;
                case Icons.Up:
                    _icon.Icon = VolumeScroller.Properties.Resources.mouse_up;
                    break;
                case Icons.Down:
                    _icon.Icon = VolumeScroller.Properties.Resources.mouse_down;
                    break;
            };

            _autoResetTimer.Enabled = false;
            _autoResetTimer.Enabled = true;
        }

        private void OnDblClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void OnAutoReset(Object source, System.Timers.ElapsedEventArgs e)
        {
            SetIcon(Icons.Normal);
        }

        public void ShowMessage(string msg)
        {
            _icon.ShowBalloonTip( 2000, "Volume scroller", msg, ToolTipIcon.Info);
        }

        public bool Visible
        {
            get { return _icon.Visible; }
            set { _icon.Visible = value; }
        }

        public bool AutoReset = true;

        private NotifyIcon _icon = new NotifyIcon();
        private System.Timers.Timer _autoResetTimer;

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                _icon.Visible = false;
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
