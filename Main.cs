﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VolumeScroller
{
    class Main : IExternalWindowObserver, IDisposable
    {
        public Main()
        {
            _keyboardHook = new KeyboardHook();
            _icon = new Icon();
            _chromeWrapper = new ExternalWindowWrapper("chrome", this);
        }

        public void OnWindowClosed()
        {
            _keyboardHook.KeyDown -= OnKeyDown;
        }

        public void OnWindowHidden()
        {
            _keyboardHook.KeyDown -= OnKeyDown;
        }

        public void OnWindowAppeared()
        {
            _keyboardHook.KeyDown -= OnKeyDown;
            _keyboardHook.KeyDown += OnKeyDown;
            if (!Properties.Settings.Default.require_scoll_lock || _keyboardHook.IsKeyPressed(Keys.Scroll))
            {
                _icon.ShowMessage("I've noticed browser is visible\nI will capture volume up/down and convert it to scroll");
            }
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (!Properties.Settings.Default.require_scoll_lock || _keyboardHook.IsKeyPressed(Keys.Scroll))
            {
                if (e.KeyCode == (Keys)Properties.Settings.Default.scroll_down_key_code)
                {
                    _chromeWrapper.SendScrollMsg(false, Properties.Settings.Default.window_x_offset, Properties.Settings.Default.window_y_offset);
                    _icon.SetIcon(Icon.Icons.Down);
                    e.SuppressKeyPress = true;
                }
                if (e.KeyCode == (Keys)Properties.Settings.Default.scroll_up_key_code)
                {
                    _chromeWrapper.SendScrollMsg(true, Properties.Settings.Default.window_x_offset, Properties.Settings.Default.window_y_offset);
                    _icon.SetIcon(Icon.Icons.Up);
                    e.SuppressKeyPress = true;
                }
            }
        }

        private ExternalWindowWrapper _chromeWrapper;
        private KeyboardHook _keyboardHook;
        private Icon _icon;

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                _keyboardHook.Dispose();
                _icon.Dispose();
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
