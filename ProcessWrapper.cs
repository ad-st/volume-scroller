﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VolumeScroller
{
    public interface IExternalWindowObserver
    {
        void OnWindowClosed();
        void OnWindowHidden();
        void OnWindowAppeared();
    }

    public class ExternalWindowWrapper
    {
        public ExternalWindowWrapper(string processName, IExternalWindowObserver observer)
        {
            _observer = observer ?? throw new ArgumentException("window observer can't be null");
            _processName = processName;

            FindWindow();
        }

        public void SendScrollMsg(bool up, Int32 x, Int32 y)
        {
            const UInt32 WM_MOUSEWHEEL = 0x020A;

            try
            {
                if (!IsIconic(_windowHandle) && GetWindowRect(_windowHandle, out RECT rect))
                {
                    UIntPtr scrollAmount = up ? (UIntPtr)0x00780000 : (UIntPtr)0xff880000;
                    IntPtr coordinates = (IntPtr)(((rect.Top & 0xffff) + x) << 16 | ((rect.Left & 0xffff) + y));
                    SendMessage(_windowHandle, WM_MOUSEWHEEL, scrollAmount, coordinates);
                    return;
                }
            }
            catch (Exception)
            {
            }
            _observer.OnWindowClosed();
        }

        private async void FindWindow()
        {
            while (true)
            {
                Process[] processes = Process.GetProcessesByName(_processName);
                foreach (Process process in processes)
                {
                    var probedHandle = process.MainWindowHandle;
                    if (probedHandle == IntPtr.Zero)
                        continue;
                    if (IsIconic(probedHandle))
                    {
                        if (_visible)
                            _observer.OnWindowHidden();
                        _visible = false;
                        continue;
                    }
                    if (!_visible)
                    {
                        _windowHandle = probedHandle;
                        _observer.OnWindowAppeared();
                        _visible = true;
                    }
                }
                await Task.Delay(2000);
            }
        }

        private IExternalWindowObserver _observer;
        private string _processName;
        private IntPtr _windowHandle = IntPtr.Zero;
        private bool _visible = false;

        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, UIntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        private static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

        [DllImport("user32.dll")]
        private static extern bool IsIconic(IntPtr hWnd);

        [StructLayout(LayoutKind.Sequential)]
        private struct RECT
        {
            public Int32 Left;
            public Int32 Top;
            public Int32 Right;
            public Int32 Bottom;
        }
    }
}
