﻿using System;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Forms;

namespace VolumeScroller
{
    public class KeyboardHook : IDisposable
    {
        public KeyboardHook()
        {
            keyboardProc = HookCallback;
            hookId = SetHook(keyboardProc);
        }

        public event KeyEventHandler KeyDown;
        public event KeyEventHandler KeyUp;

        public bool IsKeyPressed(Keys key)
        {
            return (GetKeyState((int)key) & 0x0001) != 0;
        }

        protected virtual bool OnKeyDown(KeyEventArgs e)
        {
            KeyDown?.Invoke(this, e);
            return e.SuppressKeyPress;
        }

        protected virtual bool OnKeyUp(KeyEventArgs e)
        {
            KeyUp?.Invoke(this, e);
            return e.SuppressKeyPress;
        }

        private IntPtr SetHook(LowLevelKeyboardProc proc)
        {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule)
            {
                return SetWindowsHookEx(WH_KEYBOARD_LL, proc,
                    GetModuleHandle(curModule.ModuleName), 0);
            }
        }

        private IntPtr HookCallback(int nCode, UIntPtr wParam, IntPtr lParam)
        {
            UIntPtr WM_KEYDOWN = (UIntPtr)0x0100;
            UIntPtr WM_KEYUP = (UIntPtr)0x0101;

            if (nCode >= 0 && wParam == WM_KEYDOWN)
            {
                var eventArgs = new KeyEventArgs((Keys)Marshal.ReadInt32(lParam));
                if ( OnKeyDown( eventArgs ))
                {
                    return (IntPtr)1;
                }
            }
            else if (nCode >= 0 && wParam == WM_KEYUP)
            {
                var eventArgs = new KeyEventArgs((Keys)Marshal.ReadInt32(lParam));
                if (OnKeyUp(eventArgs))
                {
                    return (IntPtr)1;
                }
            }

            return CallNextHookEx(hookId, nCode, wParam, lParam);
        }

        private IntPtr hookId;
        private static LowLevelKeyboardProc keyboardProc;
        private delegate IntPtr LowLevelKeyboardProc(int nCode, UIntPtr wParam, IntPtr lParam);
        private const int WH_KEYBOARD_LL = 13;

        [DllImport("user32.dll")]
        private static extern IntPtr SetWindowsHookEx(Int32 idHook, LowLevelKeyboardProc lpfn, IntPtr hMod, UInt32 dwThreadId);

        [DllImport("user32.dll")]
        private static extern Boolean UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll")]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, Int32 nCode, UIntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll")]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        [DllImport("user32.dll")]
        private static extern Int16 GetKeyState(Int32 keyCode);

        
        
        

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                UnhookWindowsHookEx(hookId);
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
